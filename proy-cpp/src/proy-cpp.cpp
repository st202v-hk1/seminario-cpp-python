//============================================================================
// Name        : proy-cpp.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

void pruebaTipoDatos(){
	int numeroEntero = 45;
	long long numeroEnteroLargo = 444585899965;
	float nota = 10.55;
	double distancia = 14.655889;
	string saludo = "Hola mundo";
	cout<<numeroEntero<<endl;
	cout<<numeroEnteroLargo<<endl;
	cout<<nota<<endl;
	cout<<distancia<<endl;
	cout<<saludo<<" que tal"<<endl;
}

void pruebaCondicionales(){
	int nota = 12;
	string condicion = "";
	if(nota==10){
		condicion = "B";
	}else if(nota>10){
		condicion = "A";
	}else{
		condicion = "C";
	}

	if(condicion=="A" || condicion=="B"){
		cout<<"Felicidades"<<endl;
	}else{
		cout<<"Debe mejorar"<<endl;
	}
	//Salida: Felicidades

}

void pruebaRepetitivas(){
	int contador = 0;
	int suma = 0;
	while(contador<5){
		suma+=contador;
		contador++;
	}
	cout<<"Valor final de suma: "<<suma<<endl;

	suma=0;
	for(int i=0;i<5;i++){
		suma+=i;
	}
	cout<<"Valor final de suma: "<<suma<<endl;
}

void pruebaListas(){
	int notas[4]={10,11,11,20};
	int promedios[4]={9.9,8.4,3.5,11.2};
	//imprimir(promedios,4)
	cout<<notas[3]<<endl;

	vector<int> valores;
	valores.push_back(10);
	valores.push_back(15);
	valores.push_back(8);
	valores.push_back(12);
	//imprimir(valores)

}

void pruebaMatrices(){
	int matriz[3][3] =
		{{1,2,3},{4,5,6},{7,8,9}};

	for(int i=0; i<3; i++){
		//imprimir(matriz[i]);
	}

	int suma = 0;
	for(int i=0; i<3; i++){
		for(int j=0; j<3;j++){
			cout<<"Elemento: ";
			cout<<matriz[i][j]<<endl;
			suma+=matriz[i][j];
		}
	}
	cout<<"Suma: "<<suma<<endl;

}

void pruebaCadenas(){
	string cad = "Hola mundo";
	cout<<cad[2]<<endl;
	cad+=" cruel";
	cad[5] = 'M';
	cout<<cad<<endl;
	cout<<cad.substr(5,5)<<endl;
	cout<<cad.substr(11,5)<<endl;
	cout<<cad.substr(0,4)<<endl;
	cout<<cad.find("la")<<endl;
}

int sumar(int a, int b){
	return a+b;
}

void operacion2(int a, int b,
		int resultado[]){
	resultado[0] = 2*a;
	resultado[1] = pow(b,2);
}

string concatenar(string cadena){
	return "La cadena es: "+cadena;
}

void pruebaFunciones(){
	cout<<sumar(3,5)<<endl;
	int resultado[2]={0};
	operacion2(3,6,resultado);
	cout<<resultado[0]<<",";
	cout<<resultado[1]<<endl;
	cout<<concatenar("ABC");
}

struct Persona{
	string nombre;
	string apellido;
	int edad;
	string fechaNacimiento;
	void saludar(){
		cout<<"Hola, soy"<<nombre;
		cout<<" "<<apellido<<endl;
	}
};

void pruebaRegistro(){
	struct Persona alumno;
	cout<<"Ingrese nombre: "<<endl;
	cin>>alumno.nombre;
	cout<<"Ingrese apellido: "<<endl;
	cin>>alumno.apellido;
	cout<<"Ingrese edad: "<<endl;
	cin>>alumno.edad;
	cout<<"Edad2: "<<alumno.edad+5<<endl;
	alumno.saludar();
}


int main() {
	//pruebaTipoDatos();
	//pruebaCondicionales();
	//pruebaRepetitivas();
	//pruebaListas();
	//pruebaMatrices();
	//pruebaFunciones();
	//pruebaCadenas();
	pruebaRegistro();
	return 0;
}
