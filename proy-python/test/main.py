'''
Created on May 15, 2018

@author: JC
'''

import matplotlib.pyplot as plt
import numpy as np

def pruebaTipoDatos():
    numeroEntero = 45
    numeroEnteroLargo = 444585899965
    nota = 10.55
    distancia = 14.655889
    saludo = 'Hola mundo'
    print(numeroEntero)
    print(numeroEnteroLargo)
    print(nota)
    print(distancia)
    print(saludo,"que tal")

def pruebaCondicionales():
    nota = 12
    if(nota==10):
        condicion = "B"
    elif(nota>10):
        condicion = "A"
    else:
        condicion = "C"
        
    if (condicion=="A") or (condicion=="EN B"):
        print("Felicidades")
    else:
        print("Debe mejorar")
    #Salida: Felicidades
    
def pruebaRepetitivas():
    contador = 0
    suma = 0
    while(contador<5):
        suma+=contador
        contador = contador+1
    print("Valor final de suma:",suma)
    
    contador = 0
    suma = 0
    for i in range(5): #(5,0) (5,0,1)
        suma+=i
        i = i+1
    print("Valor final de suma:",suma)
    
def pruebaCadenas():
    cad = "Hola mundo"
    print(cad[2])
    cad+=" cruel"
    #cad[5]='M'
    print(cad)
    print(cad[5:10])
    print(cad[11:])
    print(cad[:4])    
    print(cad.find("la"))
    
def pruebaMatplotlib():

    plt.ylabel('Valor de log(x)')
    plt.xlabel('Valor de x')
    
    k = np.linspace(1,10,100)
    
    plt.plot(k,np.log(k))
    plt.show()
    
    plt.ylabel('sen(x) y cos(x)')
    plt.xlabel('Valor de x')
    
    plt.plot(k,np.sin(k))
    plt.plot(k,np.cos(k))
    plt.show()

def pruebaListas():
    notas = [10,11,11,20]
    promedios = [9.9,8.4,3.5,11.2]
    print(promedios)
    print(notas[3])
    valores = []
    valores += [10,15]
    valores += [8]
    valores.append(12) #"ABC"
    print(valores);
    print([2*i for i in range(1,10) ])
    
    
def pruebaMatrices():
        
    matriz = [[1,2,3],[4,5,6],[7,8,9]]
    
    for fila in matriz:
        print('Fila',fila)
    
    suma = 0
    for fila in matriz:
        for elemento in fila:
            suma+=elemento
            print('Elemento: ',elemento)
    
    print("Suma: ",suma)
    

#prueba Diccionarios
def pruebaDiccionarios():
    diccionario = {"amo":50, "quiero":25, "desagrado":-25, "odio":-25}
    print(diccionario["odio"])
    
    persona = {'nombre':'Juan','apellido':'Perez'}
    print('Bienvenido :' + persona['nombre'] + ' ' + persona['apellido'])

    personas = [{'nombre':'Benito','apellido':'Juarez'}, {'nombre':'Juan','apellido':'Perez'}]
    
    print(personas[1]['nombre'])


def sumar(a,b):
    return a+b

def operacion2(a,b):
    return 2*a,b**2

def concatenar(cadena):
    return "La cadena es: "+cadena

def pruebaFunciones():
    print(sumar(3,5))
    print(operacion2(3,6))
    print(concatenar("ABC"))
    
class Persona:
    nombre=''
    apellido=''
    edad=0
    fechaNacimiento=''
    def saludar(self):
        print("Hola, soy",self.nombre,
              self.apellido)

def pruebaRegistro():
    alumno = Persona()
    alumno.nombre = input("Ingrese nombre: ")
    alumno.apellido = input("Ingrese apellido: ")
    alumno.edad = int(input("Ingrese edad: "))
    print("Edad2: ",alumno.edad+5)
    alumno.saludar()

# Pruebas numpy
def pruebaNumpy():
    print('En prueba numpy')
    #a = np.zeros((3,3))
   
    # Prueba ndarray
    a = np.array([[1,2,3],[4,5,6],[7,8,9]])
    print(a)
    print(10*a)
    print(np.dot(a,a))
   
    a = np.zeros((3,3))
    b = np.ones((3,3))
    a+=6
    a[1]-=-8
    print(a)
    print(b)
    print(a*b)
    print(a*b-1)
    print(np.dot(a,b))
    
    print(np.log(a))
    print(np.exp(b))
    print(np.cos(b))
    
    print(a.T)
    
if __name__ == '__main__':
    #pruebaTipoDatos()
    #pruebaCondicionales()
    #pruebaRepetitivas()
    #pruebaCadenas()
    #pruebaListas()
    #pruebaMatrices()
    #pruebaFunciones();
    pruebaRegistro()
    #pruebaMatplotlib()